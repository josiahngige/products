<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {  return view('backend.login');});

//Route::any('/reset-password/{token}', ['as' => 'reset.password', 'uses' => 'AuthenticationController@passwordReset']);
//Route::get('/verify-email/{token}', ['as' => 'verify.email', 'uses' => 'AuthenticationController@verifyEmail']);
//Route::any('/reset-invite-password/{token}',    ['as' => 'reset.invite.password', 'uses' => 'AuthenticationController@resetInvitePassword']);
//
Route::any('/backend/login', ['as' => 'login', 'uses' => 'AuthenticationController@login']);
Route::any('/backend/register', ['as' => 'register', 'uses' => 'AuthenticationController@register']);
Route::any('/backend/password-recovery',    ['as' => 'password.recovery', 'uses' => 'AuthenticationController@passwordRecovery']);
Route::any('/backend/logout', ['as' => 'logout', 'uses' => 'AuthenticationController@logout']);

Route::group(['middleware' => 'sauth', 'prefix' => 'admin'], function () {
    Route::any('/home', ['as' => 'sadmin.home', 'uses' => 'BackendController@index']);
});

Route::group(['middleware' => 'auth', 'prefix' => 'backend'], function () {
    Route::any('/home', ['as' => 'sadmin.home', 'uses' => 'SuperAdminController@index']);
});
