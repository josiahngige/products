<?php

/**
 * Description of SuperAdminController
 *
 * @author Ansel Melly <ansel@anselmelly.com>
 * @date Aug 27, 2015
 * @link http://www.anselmelly.com
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Store;
use App\Events;
use App\Posters;
use App\TicketOrder;
use App\PaymentMonitor;
use App\TicketCategories;
use App\WithdrawRequest;
use App\BankDetails;
use Illuminate\Http\Request;
use Session;
use Input;
use View;
use Mail;
use Carbon\Carbon;

class SuperAdminController extends Controller {

    public function __construct() {
        parent::__construct();
        // is this super admin have an account
        $user = auth()->user();
        $stores = Store::where('user_id', '=', $user->id)->get();
        if ($stores) {
            View::share('is_admin', TRUE);
        }
    }

    public function index(Request $request) {
        switch ($request->get('action')) {
            case 'get_stores':
                return self::getStores();
            case 'get_events':
                return self::getEvents($request->get('store'));
            case 'get_event':
                return self::getEvent($request->get('event'));
            case 'get_order_summary':
                return self::getOrderSummary($request->get('event'));
            case 'get_orders':
                return self::getEventOrders($request->get('event'));
            case 'verify-withdrawal':
                return self::verifyWithdrawal($request->get('event'));
            case 'withdrawal-history':
                return self::withrawals($request->get('event'));
            case 'search-order':
                return self::searchOrder();
            case 'complete-order':
                return self::completeOrder($request->get('order-id'));
            case 'cancel-order':
                return self::cancelOrder($request->get('order-id'));
            case 'resend-tickets':
                return self::resendTickets($request->get('order-id'));
            case 'event-tickets':
                return self::tickets($request->get('event'));
            case 'category-tickets':
                return self::categoryTickets($request->get('category'));
            case 'order-tickets':
                return self::orderTickets($request->get('order-id'));
            default:
                return self::getStores();
        }
    }

    public function orderTickets($order) {
        $the_tickets = \App\Tickets::where('order_id', '=', $order)->paginate(20);
        $the_tickets->setPath('superadmin');
        return view('sadmin.order-tickets')
                        ->withOrder($order)
                        ->withTickets($the_tickets);
    }

    public function tickets($event) {
        $the_event = Events::with('tickets')->whereId($event)->first();
        return view('sadmin.view-tickets')
                        ->with([
                            'event' => $the_event
        ]);
    }

    public function categoryTickets($ticket_category) {
        $the_tickets = \App\Tickets::where('ticket_id', '=', $ticket_category)->paginate(20);
        $the_tickets->setPath('superadmin');
        $the_category = TicketCategories::with('event_details')->where('id', '=', $ticket_category)
                ->first();
        return view('sadmin.tickets')
                        ->withTickets($the_tickets)
                        ->withCategory($the_category);
    }

    public function resendTickets($order) {
        $the_tickets = \App\Tickets::where('order_id', '=', $order)->get();
        if ($the_tickets) {
            $the_payment = PaymentMonitor::with(['order', 'order.events', 'order.events.posters', 'order.events.support'])
                    ->where('order_id', '=', $order)
                    ->first();
            if ($the_payment) {
                $the_order = $the_payment->order;
                $the_event = $the_order->events;
                $the_support = $the_event->support;
                $the_posters = $the_event->posters;
                $the_poster = NULL;
                foreach ($the_posters as $poster) {
                    if ($poster->is_main == 1) {
                        $the_poster = $poster;
                    }
                }
                $the_final_tickets = \App\Tickets::where('order_id', '=', $the_order->order_number)->get();
                foreach ($the_final_tickets as $key => $fticket) {
                    $details = TicketCategories::whereId($fticket->ticket_id)->first();
                    $the_final_tickets[$key]->{'details'} = $details;
                }
                $data = [
                    'payment' => $the_payment,
                    'event' => $the_event,
                    'poster' => $the_poster,
                    'tickets' => $the_final_tickets,
                    'order' => $the_order,
                    'support' => $the_support
                ];

                Mail::send('emails.tickets', $data, function($message)use($data, $the_final_tickets) {
                    $message->from('no-reply@mookh.net', 'Mookh');
                    $message->to($data['order']->email, $data['payment']->names);
                    foreach ($the_final_tickets as $attachement) {
                        $file = public_path() . '/' . $attachement->qr_path;
                        if (\File::exists($file)) {
                            $message->attach($file, ['as' => snake_case(strtolower($attachement->details->name)) . $attachement->qr_code . '.png']);
                        }
                    }
                    $message->subject('Your Purchased Tickets');
                });
                return redirect()->route('sadmin.home', ['action' => 'get_orders', 'event' => $the_event->id, 'state' => 'complete'])->withSuccess('The tickets have been resent');
            } else {
                return redirect()->route('sadmin.home')->withError('That Order id does not exist');
            }
        }
    }

    public function cancelOrder($order) {
        $the_order = TicketOrder::where('order_number', '=', $order)->first();
        if ($the_order) {
            $updated = $the_order->update([
                'is_processed' => 1,
                'order_state' => 'canceled'
            ]);
            if ($updated) {
                return redirect()->route('sadmin.home', ['action' => 'get_orders', 'event' => $the_order->event])
                                ->withSuccess('Order Canceled');
            }
        }
        return redirect()->back()->withError('Failed to cancel order. Please re-try');
    }

    public function completeOrder($order) {
        // check if we have already generated the codes
        $the_tickets = \App\Tickets::where('order_id', '=', $order)->get();
        if ($the_tickets->count() > 0) {
            return redirect()->back()->withInfo('Tickets have already been generated');
        }
        $the_payment = PaymentMonitor::with(['order', 'order.events', 'order.events.posters', 'order.events.support'])
                ->where('order_id', '=', $order)
                ->first();
        if ($the_payment) {
            $the_order = $the_payment->order;
            $the_event = $the_order->events;
            $the_support = $the_event->support;
            $the_posters = $the_event->posters;
            $the_poster = NULL;
            foreach ($the_posters as $poster) {
                if ($poster->is_main == 1) {
                    $the_poster = $poster;
                }
            }
            $the_tickets = unserialize($the_order->tickets);
            foreach ($the_tickets as $ticket) {
                $this->the_attachments[] = self::saveTicket($the_order, $ticket);
            }

            $the_final_tickets = \App\Tickets::where('order_id', '=', $the_order->order_number)->get();
            foreach ($the_final_tickets as $key => $fticket) {
                $details = TicketCategories::whereId($fticket->ticket_id)->first();
                $the_final_tickets[$key]->{'details'} = $details;
            }
            $data = [
                'payment' => $the_payment,
                'event' => $the_event,
                'poster' => $the_poster,
                'tickets' => $the_final_tickets,
                'order' => $the_order,
                'support' => $the_support
            ];

            Mail::send('emails.tickets', $data, function($message)use($data, $the_final_tickets) {
                $message->from('no-reply@mookh.net', 'Mookh');
                $message->to($data['order']->email, $data['payment']->names);
                foreach ($the_final_tickets as $attachement) {
                    $file = public_path() . '/' . $attachement->qr_path;
                    if (\File::exists($file)) {
                        $message->attach($file, ['as' => snake_case(strtolower($attachement->details->name)) . $attachement->qr_code . '.png']);
                    }
                }
                $message->subject('Your Purchased Tickets');
            });
            return self::orderSuccess($data);
        }
    }

    public function saveTicket($the_order, $ticket) {
        $the_ticket = TicketCategories::whereId($ticket['ticket'])->first();
        if ($the_ticket) {
            for ($i = 0; $i < $ticket['qty']; $i++) {
                $qr_code = self::generateRandomString(32);
                $qr_image = $qr_code . '.png';
                $qr_path = public_path() . '/qrcodes/' . $qr_image;
                $url = route('qr.verify', $qr_code);
                $qr_generated = self::generateQr($url, $qr_path);
                $t = new \App\Tickets();
                $t->order_id = $the_order->order_number;
                $t->qr_code = $qr_code;
                $t->qr_generated = $qr_generated;
                $t->qr_path = 'qrcodes/' . $qr_image;
                $t->ticket_id = $ticket['ticket'];
                $saved = $t->save();
                if ($saved) {
                    // run the reducing here
                    $ticket_monitor = \App\TicketMonitor::where('ticket_id', '=', $ticket['ticket'])->first();
                    if ($ticket_monitor) {
                        $remaining_tickets = (int) $ticket_monitor->remaining_tickets - 1;
                        $sold_tickets = (int) $ticket_monitor->total_tickets - (int) $remaining_tickets;
                        $ticket_monitor->update([
                            'sold_tickets' => $sold_tickets,
                            'remaining_tickets' => $remaining_tickets
                        ]);
                    }
                }
            }
            return $qr_path;
        }
        return;
    }

    public function orderSuccess($data) {
        $the_order = TicketOrder::where('order_number', '=', $data['order']->order_number)
                ->first();
        $the_payment = PaymentMonitor::where('order_id', '=', $data['order']->order_number)->first();
        if ($the_order && $the_payment) {
            // mark this order as complete
            $the_order->update([
                'order_state' => 'complete',
                'is_processed' => '1'
            ]);
            $the_payment->update([
                'payment_state' => 'paid',
                'amount_paid' => $the_payment->amount
            ]);
            return redirect()->route('sadmin.home', ['action' => 'get_orders', 'event' => $the_order->event])
                            ->withSuccess('Order completed');
        } else {
            return redirect()->route('sadmin.home')->withInfo('Order ID is invalid');
        }
    }

    public function generateQr($url, $path) {
        \Artisan::call('qrencode', [
            'url' => $url,
            'path' => $path
        ]);
        if (!\File::exists($path)) {
            self::generateQr($url, $path);
        } else {
            return true;
        }
    }

    public function searchOrder() {
        $input = Input::except('_token', 'action');
        $orders = TicketOrder::where('order_number', '=', $input['search'])
                ->paginate(20);

        $orders->setPath('superadmin');

        $event = $input['event'];

        $the_event = Events::where('id', '=', $event)->first();
        if ($orders && $the_event) {
            foreach ($orders as $key => $order) {
                $the_tickets = [];
                $tickets = unserialize($order->tickets);
                if (is_array($tickets)) {
                    foreach ($tickets as $ticket) {
                        $ticket_details = TicketCategories::where('id', '=', $ticket['ticket'])->first();
                        if ($ticket_details) {
                            $ticket_details->{'qty'} = $ticket['qty'];
                            $the_tickets[] = $ticket_details;
                        }
                    }
                }
                $order->{'tdetails'} = $the_tickets;
                $the_payment = PaymentMonitor::where('order_id', '=', $order->order_number)->first();
                $order->payment = $the_payment;
                $orders[$key] = $order;
            }
        }
        return view('sadmin.orders')
                        ->withOrders($orders)
                        ->withEvent($the_event);
    }

    public function withrawals($event) {
        $the_requests = WithdrawRequest::where('event', '=', $event)
                        ->where('approval', '=', 1)->get();
        $the_event = Events::where('id', '=', $event)->first();
        if ($the_event && $the_requests) {
            return view('sadmin.withdrawals')->with([
                        'requests' => $the_requests,
                        'event' => $the_event
            ]);
        }
    }

    public function verifyWithdrawal($event) {

        $orders = TicketOrder::where('event', '=', $event)->get();
        $the_event = Events::where('id', '=', $event)->first();

        $the_request = WithdrawRequest::where('event', '=', $event)
                ->where('approval', '=', 0)
                ->first();

        if ($the_event && $the_request) {
            if (Input::isMethod('post')) {
                $validate = \Validator::make(Input::except('_token'), [
                            'notes' => 'required'
                ]);
                if ($validate->fails()) {
                    return redirect()->back()->withErrors($validate)->withInput()->withError('Your should put in some notes.');
                } else {
                    $input = Input::only('event', 'notes');
                    $c = new Carbon();
                    $input['approved_at'] = $c->createFromTimestamp(time());
                    $input['approval'] = 1;
                    $saved = $the_request->update($input);
                    if ($saved) {
                        return redirect()->route('sadmin.home', ['action' => 'withdrawal-history', 'event' => $the_event->id])->withSuccess('Request approved');
                    }
                }
            }
            $bank_details = BankDetails::where('store', '=', $the_event->store)->first();
            $the_store = Store::where('id', '=', $the_event->store)->first();
            return view('sadmin.approve-withdrawal')->with([
                        'event' => $the_event,
                        'bank_details' => $bank_details,
                        'request' => $the_request,
                        'store' => $the_store
                    ])->with(self::getAmountInAccount($orders, $the_event));
        }
    }

    public function getEvent($event) {
        $this_event = Events::whereId($event)->first();
        if ($this_event) {
            $posters = Posters::whereEventId($this_event->id)
                    ->get();
            $tickets = TicketCategories::where('event', '=', $this_event->id)
                    ->get();
            $coordinates = $this_event->coordinates;
            if ($coordinates != NULL || $coordinates != "") {
                $split_coodes = explode(',', $coordinates);
                if (count($split_coodes) == 2) {
                    $this_event->latitude = $split_coodes[0];
                    $this_event->longitude = $split_coodes[1];
                } else {
                    $this_event->coordinates = NULL;
                }
            } else {
                $this_event->coordinates = NULL;
            }
            return view('sadmin.event')
                            ->withEvent($this_event)
                            ->withScripts([
                                "https://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7",
                                'plugins/fancybox/jquery.fancybox.js',
                                'js/image-fancy.js',
                                'js/maplace-0.1.3.min.js'
                            ])
                            ->withStyles(['plugins/fancybox/jquery.fancybox.css'])
                            ->withTickets($tickets)
                            ->withDistLocation(true)
                            ->withPosters($posters);
        }

        return redirect()->isNotFound()
                        ->withError('The page you are looking for is missing.');
    }

    protected function getEventOrders($event) {
        $orders = TicketOrder::where('event', '=', $event)->paginate(20);
        if (Input::has('state')) {
            if (\Input::get('state') == 'complete') {
                $orders = TicketOrder::whereHas('payment', function($query) {
                            $query->where('payment_state', '=', 'paid');
                        })->where('event', '=', $event)
                        ->paginate(20);
            }
            if (\Input::get('state') == 'partial') {
                $orders = TicketOrder::whereHas('payment', function($query) {
                            $query->where('payment_state', '=', 'partial');
                        })->where('event', '=', $event)
                        ->paginate(20);
            }
            if (\Input::get('state') == 'canceled') {
                $orders = TicketOrder::where('event', '=', $event)->whereOrderState('canceled')
                        ->paginate(20);
            }
            if (\Input::get('state') == 'incomplete') {
                $orders = TicketOrder::where('event', '=', $event)->whereOrderState('in_progress')
                        ->paginate(20);
            }
        }
        $orders->setPath('superadmin');

        $the_event = Events::where('id', '=', $event)->first();
        if ($orders && $the_event) {
            foreach ($orders as $key => $order) {
                $the_tickets = [];
                $tickets = unserialize($order->tickets);
                if (is_array($tickets)) {
                    foreach ($tickets as $ticket) {
                        $ticket_details = TicketCategories::where('id', '=', $ticket['ticket'])->first();
                        if ($ticket_details) {
                            $ticket_details->{'qty'} = $ticket['qty'];
                            $the_tickets[] = $ticket_details;
                        }
                    }
                }
                $order->{'tdetails'} = $the_tickets;
                $the_payment = PaymentMonitor::where('order_id', '=', $order->order_number)->first();
                $order->payment = $the_payment;
                $orders[$key] = $order;
            }
        }
        return view('sadmin.orders')
                        ->withOrders($orders)
                        ->withEvent($the_event);
    }

    public function getOrderSummary($event) {
        $orders = TicketOrder::where('event', '=', $event)->get();
        $the_event = Events::where('id', '=', $event)->first();
        $the_ticket_categories = TicketCategories::with('monitor')
                        ->where('event', '=', $event)->get();
        if ($orders && $the_event && $the_ticket_categories) {
            return view('sadmin.order-summary')
                            ->withTicketCategories($the_ticket_categories)
                            ->withEvent($the_event)
                            ->with(self::getAmountInAccount($orders, $the_event));
        }
    }

    protected function getAmountInAccount($orders, $the_event) {

        $complete_order_total = 0;
        $partial_order_total = 0;
        $number_of_canceled_orders = 0;
        $number_of_partial_orders = 0;
        $number_of_complete_orders = 0;
        $number_of_incomplete_orders = 0;

        if ($orders && $the_event) {
            $approved_requests = WithdrawRequest::where('event', '=', $the_event->id)
                    ->where('approval', '=', 1)
                    ->get();
            $approved_amount = 0;
            foreach ($approved_requests as $req) {
                $approved_amount += $req->amount;
            }
            foreach ($orders as $key => $order) {
                $the_payment = PaymentMonitor::where('order_id', '=', $order->order_number)->first();
                if ($the_payment != NULL) {
                    if ($the_payment->payment_state == 'paid') {
                        $complete_order_total += $the_payment->amount_paid;
                        $number_of_complete_orders ++;
                    }
                    if ($the_payment->payment_state == 'partial') {
                        $partial_order_total += $the_payment->amount_paid;
                        $number_of_partial_orders ++;
                    }
                    if ($order->order_state == 'canceled') {
                        $number_of_canceled_orders ++;
                    }
                    if ($order->order_state == 'in_progress') {
                        $number_of_incomplete_orders ++;
                    }
                }
            }
            return [
                'total_sales' => (int) $complete_order_total + (int) $partial_order_total,
                'complete_order_total' => $complete_order_total,
                'partial_order_total' => $partial_order_total,
                'number_of_canceled_orders' => $number_of_canceled_orders,
                'number_of_partial_orders' => $number_of_partial_orders,
                'number_of_complete_orders' => $number_of_complete_orders,
                'approved_amount' => $approved_amount,
                'available_balance' => ((int) $complete_order_total + (int) $partial_order_total) - (int) $approved_amount,
                'number_of_incomplete_orders' => $number_of_incomplete_orders
            ];
        }
        return [];
    }

    protected function getStores() {
        $the_stores = Store::with('storeuser')->get();
        return view('sadmin.all-stores')
                        ->withStores($the_stores);
    }

    protected function getEvents($store) {
        $store_events = Events::with('tickets', 'categories', 'posters')->where('store', '=', $store)->get();
        $the_store = Store::find($store);
        if ($store_events && $the_store) {
            return view('sadmin.store-events')
                            ->withStore($the_store)
                            ->withEvents($store_events);
        }
    }

    protected function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
