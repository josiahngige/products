<?php

/**
 * Description of AuthenticationController
 *
 * @author Ansel Melly <ansel@anselmelly.com>
 * @date Jul 29, 2015
 * @link http://www.anselmelly.com
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;
use Hash;
use Mail;
use App\PasswordReset;

class AuthenticationController extends Controller
{


    public function register(Request $request)
    {

        if ($request->isMethod('post')) {
            $validator = Validator::make($request->except('_token'), [
                'names'       => 'required',
                'email'       => 'required|email|unique:users',
                'phone'       => 'required|numeric',
                'password'    => 'required|min:6',
                'cnfpassword' => 'required|same:password',
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withInfo('Please Correct the errors below')
                    ->withInput()
                    ->withErrors($validator);
            } else {
                $user = new User();
                $user->names = $request->get('names');
                $user->email = $request->get('email');
                $user->phone = $request->get('phone');
                $user->password = Hash::make($request->get('password'));
                $randomToken = self::generateRandomString(10);
                $user->role = 'admin';
                $user->vtoken = $randomToken;
                $user->status = 'not_verified';
                $saved = $user->save();
                if ($saved) {
                    Mail::send('emails.register',
                        ['names' => $user->names, 'url' => route('verify.email', $user->vtoken)],
                        function ($message) use ($user) {
                            $message->from('no-reply@mymookh.com', 'Mookh');
                            $message->to($user->email, $user->names);
                            $message->subject('Email Verification');
                        });

                    return redirect()->route('login')
                        ->withSuccess('We have sent you an email for verification');
                }
            }
        }

        return view('admin.apages.register');
    }

    public function verifyEmail($vtoken)
    {
        $user = User::where('vtoken', '=', $vtoken)
            ->where('status', '=', 'not_verified')
            ->first();
        if ($user) {
            $user = User::find($user->id);
            $user->status = 'email_verified';
            $updated = $user->update();
            if ($updated) {
                return redirect()->route('login')->withSuccess('Email successfully verified. Please login to continue');
            }
        } else {
            $user = User::where('vtoken', '=', $vtoken)
                ->where('status', '=', 'not_verified|reset_p')
                ->first();
            if ($user) {
                // prompt the user to change their password
                return redirect()->route('reset.invite.password', $vtoken);
            } else {
                return redirect()->route('login')->withInfo('Account already verified your account or invalid token');
            }
        }
    }

    public function resetInvitePassword(Request $request, $vtoken)
    {
        $user = User::where('vtoken', '=', $vtoken)
            ->where('status', '=', 'not_verified|reset_p')
            ->first();
        if ($user) {
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->except('_token'), [
                    'password'    => 'required',
                    'cnfpassword' => 'required|same:password',
                ]);
                if ($validator->fails()) {
                    return redirect()->back()->withError('Please rectify the errors below')
                        ->withInput()->withErrors($validator);
                } else {
                    $pass_changed = $user->update([
                        'password' => Hash::make($request->get('password')),
                        'status'   => 'verified',
                    ]);
                    if ($pass_changed) {
                        return redirect()->route('login')
                            ->withSuccess('Email verified. You can now Login');
                    } else {
                        return redirect()->back()->withError('Failed to reset password. Please re-try');
                    }
                }
            }

            return view('admin.apages.set-invite-password')->withUser($user);
        }
    }

    public function passwordRecovery(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->except('_token'), [
                'email' => 'email|required',
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withError('Invalid Email format')
                    ->withInput()
                    ->withErrors($validator);
            } else {
                $recovery = new PasswordReset();
                $token = self::generateRandomString(16);
                $recovery->email = $request->get('email');
                $recovery->token = $token;
                $saved = $recovery->save();
                if ($saved) {
                    // send this guy and email for password recovery
                    Mail::send('emails.password-recovery', ['url' => route('reset.password', $recovery->token)],
                        function ($message) use ($recovery) {
                            $message->from('no-reply@mookh.net', 'Mookh');
                            $message->to($recovery->email);
                            $message->subject('Password Recover');
                        });

                    return redirect()
                        ->route('login')
                        ->withSuccess('Please check your email');
                } else {
                    return redirect()->back()->withError('Failed to reset password. Please try again');
                }
            }

            // mail this guy on the sending stuff
        }

        return view('admin.apages.password-recovery');
    }

    public function passwordReset(Request $request, $token)
    {
        $passChange = PasswordReset::where('token', '=', $token)
            ->where('used', '=', 0);
        if ($passChange) {
            if ($request->isMethod('post')) {
                // do some saving stuff here
                $validator = Validator::make($request->except('_token'), [
                    'password'    => 'required',
                    'cnfpassword' => 'required|same:password',
                ]);
                if ($validator->fails()) {
                    return redirect()->back()->withError('Please rectify the errors below')
                        ->withInput()->withErrors($validator);
                } else {
                    $pass_change_data = $passChange->first();
                    $user = User::where('email', '=', $pass_change_data->email);
                    if ($user) {
                        $updated = $user->update(['password' => Hash::make($request->get('password'))]);
                        if ($updated) {
                            $passChange->update(['used' => 1]);

                            return redirect()->route('login')->withSuccess('Password Changed.');
                        }

                        return redirect()->route('login')->withInfo('Internal Error. Please try again');
                    }

                    return redirect()->route('login')->withInfo('Internal Error. Please try again');
                    // some error where we cannot find user.. Very rare
                }
            }

            return view('admin.apages.reset-password')
                ->withToken($token);
            //give the guy the form to change the password
        } else {
            return redirect()->route('login')->withInfo('It seems you have already used this request. Please try again');
            // tell the guy that they have already used the token and should request a new one
        }
    }

    public function login(Request $request)
    {
        auth()->logout();
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->except('_token'), [
                'username'    => 'required|email',
                'password' => 'required|min:6',
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withError('Errors Encounterd. Please check');
            } else {
                $authenticated = auth()->attempt([
                    'email'    => $request->get('username'),
                    'password' => $request->get('password'),
                ]);
                if ($authenticated) {
                    if (auth()->user()->role == 'superadmin') {
                        return redirect()->intended('/superadmin/home');
                    } else {
                        if (auth()->user()->role == 'admin') {
                            return redirect()->intended('/admin/home');
                        } else {
                            return redirect()->intended('/admin/home');
                        }
                    }
                } else {
                    return redirect()->back()->withError('Invalid User credentials. Please try again');
                }
            }
        }

        return view('admin.apages.login');
    }

    public function logout()
    {
        auth()->logout();

        return redirect('https://mymookh.com');
    }

    protected function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[ rand(0, $charactersLength - 1) ];
        }

        return $randomString;
    }

}
