<?php

/**
 * Description of settings
 *
 * @author Ansel Melly <ansel@anselmelly.com>
 * @date Jul 31, 2015
 * @link http://www.anselmelly.com
 */
return [
    'sms_sync' => [
        'url' => 'http://mookh.net/sms'
    ],
    'music_sms' => [
        'url' => 'http://mookh.net/music/sms-sync'
    ],
    'fb_store_settings' => [
        'fb_app_id' => '131155550557275',
        'fb_page_url' => 'https://www.facebook.com/dialog/pagetab'
    ],
    'mpesa_settings' => [
        'url' => 'https://www.safaricom.co.ke/mpesa_online/lnmo_checkout_server.php?wsdl',
        'passkey' => '6cc5767802cfacef7ae22440100a6b71eb1277981393dfc4d5f13d4fb1e65a5d',
        'merchant_id' => '935250',
        'server_location' => 'https://www.safaricom.co.ke/mpesa_online/lnmo_checkout_server.php',
    ],
    'google' => [
        'api_key' => 'AIzaSyBLnrr2DG7qEHMLOUv9_p7EIOcYZICssUk'
    ],
    'yahoo_url' => 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22USDKES%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=',
];
