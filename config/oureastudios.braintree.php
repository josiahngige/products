<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Environment
    |--------------------------------------------------------------------------
    |
    | Please provide the environment you would like to use for braintree.
    | This can be either 'sandbox' or 'production'.
    |
    */
    'environment'             => 'production',

    /*
    |--------------------------------------------------------------------------
    | Merchant ID
    |--------------------------------------------------------------------------
    |
    | Please provide your Merchant ID.
    |
    */
    'merchantId'              => env('BRAINTREE_MERCHANT'),

    /*
    |--------------------------------------------------------------------------
    | Public Key
    |--------------------------------------------------------------------------
    |
    | Please provide your Public Key.
    |
    */
    'publicKey'               => env('BRAINTREE_PUBLIC'),

    /*
    |--------------------------------------------------------------------------
    | Private Key
    |--------------------------------------------------------------------------
    |
    | Please provide your Private Key.
    |
    */
    'privateKey'              => env('BRAINTREE_PRIVATE'),

    /*
    |--------------------------------------------------------------------------
    | Client Side Encryption Key
    |--------------------------------------------------------------------------
    |
    | Please provide your CSE Key.
    |
    */
    'clientSideEncryptionKey' => env('BRAINTREE_CES'),

    /**
     * If set to True, Subscriptions with Past Due will be considered Enabled.
     * You have to cancel subscription to them in order to block access
     */
    'allowAccessForPastDue'   => true,

    /**
     * Grace period is the time before the paid billing cycle will end.
     * If set to True, Cancelled Subscription will be considered Enabled until end of the last billing cycle.
     */
    'allowGracePeriod'        => true,

];