<div class="static-sidebar-wrapper sidebar-midnightblue">
    <div class="static-sidebar">
        <div class="sidebar">
            <div class="widget">
                <div class="widget-body">
                    <div class="userinfo">
                        <div class="avatar">
                            <?php $avatar = auth()->user()->avatar;?>
                                <a href="{!!route('admin.users.profile',auth()->user()->id)!!}" >
                                    <img src="{{$avatar or url('admin/assets/img/profile44.svg')}}"
                                 class=" img-responsive img-circle">
                                </a>
                        </div>
                        <div class="info">
                            <span class="username">{{auth()->user()->names}}</span>
                            <span class="useremail">{{auth()->user()->email}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget stay-on-collapse" id="widget-sidebar">
                <nav role="navigation" class="widget-body">
                    <ul class="acc-menu">
                        <li class="nav-separator"><span>Explore</span></li>
                        <li><a href="{!! url('admin/home') !!}"><i class="ti ti-home"></i><span>Dashboard </span></a>
                        </li>

                        <?php
                        if ( ! isset( $store_id )) {
                            if (Session::has('store_id')) {
                                $store_id = Session::get('store_id');
                            }
                        }
                        ?>

                        @if((auth()->user()->role =="admin") && isset($event->id) && ($store_id !=''))
                            <li><a href="javascript:;"><i
                                            class="ti ti-shopping-cart-full"></i><span> Orders </span></a>
                                <ul class="acc-menu">
                                    <li>
                                        <a href="{!!route('admin.event.orders',$event->id)!!}" >
                                             All Orders
                                            <i class="ti ti-view-list pull-right"></i> </a>
                                    </li>
                                    <li>
                                        <a href='{!! route('admin.event.orders',$event->id).'?state=complete' !!}'>
                                            Complete Orders
                                            <i class="ti ti-shopping-cart-full pull-right"></i></a>
                                    </li>
                                    <li>
                                        <a href='{!! route('admin.event.orders',$event->id).'?state=partial' !!}'>
                                            Partial Orders
                                             <i class="ti pull-right   ti-check"></i> </a>
                                    </li>
                                    <li>
                                        <a href='{!! route('admin.event.orders',$event->id).'?state=canceled' !!}'>
                                            Canceled Orders
                                            <i class="ti  ti-reload pull-right"></i>   </a>
                                    </li>
                                    <li>
                                        <a href='{!! route('admin.event.orders',$event->id).'?state=incomplete' !!}'>
                                            Incomplete Orders
                                            <i class="ti  ti-reload pull-right"></i></a>
                                    </li>

                                </ul>
                            </li>

                            <li> <a href="{!! route('admin.event.withraw',$event->id) !!}">
                                    <i class="ti  ti-import"></i> <span> Withdraw Requests </span>

                                </a>
                            </li>
                            {{--<li>--}}
                                {{--<a href='{!! url("backend/$store_id/finances") !!}'>--}}
                                    {{--<i class="fa fa-money"></i><span>Finances</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        @endif


                        @if( auth()->user()->role =="admin" )
                            <li> <a href="{!! route('admin.reports') !!}">
                                        <i class="ti  ti-import"></i><span>Reports</span>
                                    </a>
                            </li>
                        @endif

                        @if (auth()->user()->role =="admin")
                            <li><a href="javascript:;"><i class="ti ti-user"></i><span>User Management</span></a>
                                <ul class="acc-menu">
                                    <li><a href="{!!route('admin.users.profile',auth()->user()->id)!!}"><i
                                                    class="ti pull-right ti-user"></i>Manage</a>
                                    </li>
                                </ul>
                            </li>
                        @endif

                    </ul>
                </nav>
            </div>

        </div>
    </div>
</div>