
@if (session('message'))
    <script>
        $(document).ready(function () {
            {{--new PNotify({--}}
                {{--text: '{{ session('message') }}',--}}
                {{--icon: 'ti ti-info-alt',--}}
                {{--type: 'success',--}}
                {{--styling: 'fontawesome'--}}
            {{--});--}}
            new toastr.success('{{ session('message') }}', 'info', {timeOut: 5000});
        });
    </script>
@endif

@if (session('error'))
    <script>
        $(document).ready(function () {
            {{--new PNotify({--}}
                {{--text: '{{ session('error') }}',--}}
                {{--icon: 'ti ti-info-alt',--}}
                {{--type: 'error',--}}
                {{--styling: 'fontawesome'--}}
            {{--});--}}
            new toastr.error('{{ session('error') }}', 'error', {timeOut: 5000});
        });
    </script>
@endif

@if (session('success'))
    <script>
        $(document).ready(function () {
            {{--new PNotify({--}}
                {{--text: '{{ session('success') }}',--}}
                {{--icon: 'ti ti-info-alt',--}}
                {{--type: 'success',--}}
                {{--styling: 'fontawesome'--}}
            {{--});--}}
            new toastr.success('{{ session('success') }}', 'info', {timeOut: 5000});
        });
    </script>
@endif

@if (session('warning'))
    <script>
        $(document).ready(function () {
            {{--new PNotify({--}}
                {{--text: '{{ session('warning') }}',--}}
                {{--icon: 'ti ti-info-alt',--}}
                {{--type: 'warning',--}}
                {{--styling: 'fontawesome'--}}
            {{--});--}}
            new toastr.warning('{{ session('warning') }}', 'warning', {timeOut: 5000});
        });
    </script>
@endif

@if (session('info'))
    <script>
        $(document).ready(function () {
            {{--new PNotify({--}}
                {{--text: '{{ session('info') }}',--}}
                {{--icon: 'ti ti-info-alt',--}}
                {{--type: 'info',--}}
                {{--styling: 'fontawesome'--}}
            {{--});--}}
            new toastr.info('{{ session('info') }}', 'info', {timeOut: 5000});
        });
    </script>
@endif