@extends('layouts.login')

@section('header-styles')

@endsection

@section('content')

    <div class="menu-toggler sidebar-toggler"></div>
    <!-- END SIDEBAR TOGGLER BUTTON -->
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="index.html">
            <img src="assets/layouts/layout/img/logo-dark.png" style="width:15% !important; " alt="" /> </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">

        {!! Form::open(['route'=>'login', 'class' => 'login-form', 'role' => 'form']) !!}

        <h3 class="form-title font-black">Sign In</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span> Enter any username and password. </span>
        </div>
        @include('partials.messages')
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            {!! Form::input('text', 'username', null, ['class' => 'form-control form-control-solid placeholder-no-fix',
             'placeholder'=>'username','autocomplete'=>'off']) !!}
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            {!! Form::input('password', 'password', null, ['class' => 'form-control form-control-solid placeholder-no-fix',
            'placeholder'=>'password', 'autocomplete'=>'off']) !!}
        </div>
        <div class="form-actions">
            {!! Form::submit('log in', ['class' => 'btn black uppercase' ]) !!}

            <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
        </div>
        <div class="login-options">
            <h4>Or login with</h4>
            <ul class="social-icons">
                <li>
                    <a class="social-icon-color facebook" data-original-title="facebook" href="javascript:;"></a>
                </li>
                <li>
                    <a class="social-icon-color twitter" data-original-title="Twitter" href="javascript:;"></a>
                </li>
                <li>
                    <a class="social-icon-color googleplus" data-original-title="Goole Plus" href="javascript:;"></a>
                </li>
                <li>
                    <a class="social-icon-color linkedin" data-original-title="Linkedin" href="javascript:;"></a>
                </li>
            </ul>
        </div>
        <div class="create-account">
            <p>
                <a href="javascript:;" id="register-btn" class="uppercase">Create an account</a>
            </p>
        </div>

        {!! Form::close() !!}


        {!! Form::open(['route'=>'password.recovery', 'class' => 'forget-form', 'role' => 'form']) !!}

        <h3 class="font-black">Forget Password ?</h3>
        <p> Enter your e-mail address below to reset your password. </p>
        <div class="form-group">
            {!! Form::input('email', 'email', null, ['class' => 'form-control placeholder-no-fix',
             'placeholder'=>'email','autocomplete'=>'off']) !!}
        </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn btn-default">Back</button>
            {!! Form::submit('Get Password', ['class' => 'btn black uppercase pull-right' ]) !!}
        </div>

        {!! Form::close() !!}

        {!! Form::open(['route'=>'register', 'class' => 'register-form', 'role' => 'form']) !!}

        <h3 class="font-black">Sign Up</h3>
        <p class="hint"> Enter your personal details below: </p>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Full Name</label>
            {{--<input class="form-control placeholder-no-fix" type="text" placeholder="Full Name" name="fullname" />--}}
            {!! Form::input('text', 'fullname', null, ['class' => 'form-control placeholder-no-fix',
             'placeholder'=>'full names','autocomplete'=>'off']) !!}
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            {{--<input class="form-control placeholder-no-fix" type="email" placeholder="Email" name="email" />--}}
            {!! Form::input('email', 'email', null, ['class' => 'form-control placeholder-no-fix',
             'placeholder'=>'email','autocomplete'=>'off']) !!}
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Phone</label>
            {{--<input class="form-control placeholder-no-fix" type="text" placeholder="Phone" name="phone" />--}}
            {!! Form::input('email', 'phone', null, ['class' => 'form-control placeholder-no-fix',
                 'placeholder'=>'phone','autocomplete'=>'off']) !!}
        </div>

        <p class="hint"> Enter your account details below: </p>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            {{--<input class="form-control placeholder-no-fix" type="password" autocomplete="off"  placeholder="Password" name="password" />--}}
            {!! Form::input('password', 'password', null, ['class' => 'form-control placeholder-no-fix',
             'placeholder'=>'password','autocomplete'=>'off','id'=>'register_password']) !!}
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
            {{--<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="" />--}}
            {!! Form::input('password', 'rpassword', null, ['class' => 'form-control placeholder-no-fix',
                 'placeholder'=>'Re-type your password','autocomplete'=>'off','id'=>'register_password']) !!}
        </div>
        <div class="form-group margin-top-20 margin-bottom-20">
            <label class="check">
                <input type="checkbox" name="tnc" /> I agree to the
                <a href="javascript:;"> Terms of Service </a> &
                <a href="javascript:;"> Privacy Policy </a>
            </label>
            <div id="register_tnc_error"> </div>
        </div>
        <div class="form-actions">
            <button type="button" id="register-back-btn" class="btn btn-default">Back</button>
            {{--<button type="submit"  class="btn btn-success uppercase pull-right">Submit</button>--}}
            {!! Form::submit('Create', ['class' => 'btn black uppercase pull-right','id'=>'register-submit-btn' ]) !!}
        </div>

        {!! Form::close() !!}


    </div>
    <div class="copyright"> <?php echo date('Y');?> © Mookh. </div>

@endsection

@section('footer-scripts')

@endsection