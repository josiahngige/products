<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->string('location');
			$table->string('store');
			$table->string('user_id');
			$table->string('category');
			$table->float('price');
			$table->integer('price_negotiable');
			$table->float('promotional_price');
			$table->date('promotional_start_date')->nullable();
			$table->date('promotional_end_date')->nullable();
			$table->float('shipping_price');
			$table->float('days_delivery');
			$table->integer('seller_type');
			$table->integer('quantity');
			$table->text('description');	
			$table->text('image_1');	
			$table->text('image_2');	
			$table->text('image_3');	
			$table->text('image_4');	
			$table->text('image_5');	
			$table->text('image_6');			
			$table->boolean('published');
			$table->string('number_of _previews');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }
}
