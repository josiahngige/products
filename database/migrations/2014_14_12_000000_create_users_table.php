<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateUsersTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('phone');
                $table->integer('created_by');
                $table->string('email')->unique();
				$table->string('role');
                $table->string('password', 255);
                $table->float('balance')->default(0.00);
                $table->boolean('confirmed')->default(0);
                $table->string('confirmation_code')->nullable();
                $table->string('avatar', 255)->nullable();
                $table->rememberToken();
                $table->timestamps();
            });
        }


        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::drop('users');
        }
    }
